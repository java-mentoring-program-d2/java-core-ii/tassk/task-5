package vehicles;

public class OrdinaryBus extends Vehicle{

    public OrdinaryBus(int mass, int maxSpeed, int capacity) {
        super(mass, maxSpeed, capacity);
    }
    //Overriding getType() method from Vehicle class
    @Override
    EnergyType getType() {
        return EnergyType.GASOLINE;
    }

}
