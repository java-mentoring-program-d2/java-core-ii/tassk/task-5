package vehicles;

public class ElectricBus extends Vehicle {
     public ElectricBus(int mass, int maxSpeed, int capacity) {
        super(mass, maxSpeed, capacity);
    }
    //Overriding getType() method from Vehicle class
    @Override
    EnergyType getType() {
        return EnergyType.ELECTRICITY;
    }
}
