package annotations.processors;

import annotations.ProdCode;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

public class ProdRunner {
    private static final String START_PACKAGE = String.format("src%smain%sjava", File.separator, File.separator);
    private static final Path ORIGINAL_PATH = Paths.get(START_PACKAGE);
    private static final FileVisitor visitor = new ProdRunner.FileVisitor();

    public static void handle() throws IOException {
        Files.walkFileTree(ORIGINAL_PATH, visitor);
    }

    static class FileVisitor extends SimpleFileVisitor<Path> {
        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            try {
                String fullName = file.toAbsolutePath().toString();
                String className = fullName.substring(fullName.indexOf(START_PACKAGE) + START_PACKAGE.length() + 1, fullName.length() - 5);

                Class<?> aClass = Class.forName(className.replaceAll("[\\\\]", "."));

                Method[] methods = aClass.getDeclaredMethods();
                for (Method method : methods) {
                    if (method.isAnnotationPresent(ProdCode.class) && method.getParameters().length == 0) {
                        if (!method.canAccess(aClass.newInstance())) {
                            method.setAccessible(true);
                        }
                        method.invoke(aClass.newInstance());
                    }
                }

            } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
            }
            return FileVisitResult.CONTINUE;
        }
    }
}
