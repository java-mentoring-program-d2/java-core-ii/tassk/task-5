package annotations.processors;

import annotations.ThisCodeSmells;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ThisCodeSmellsHandler {
    private static final String START_PACKAGE = String.format("src%smain%sjava", File.separator, File.separator);
    private static final Path ORIGINAL_PATH = Paths.get(START_PACKAGE);
    private static final FileVisitor visitor = new FileVisitor();

    public static void handle() throws IOException {
        Files.walkFileTree(ORIGINAL_PATH, visitor);
        printAllCodeSmellZones();
    }

    private static void printAllCodeSmellZones() {
        for (Map.Entry<String, Map<String, List<String>>> entry : FileVisitor.superMap.entrySet()) {
            System.out.println("Reviewer: " + entry.getKey() + " {");
            for (Map.Entry<String, List<String>> entry2: entry.getValue().entrySet()) {
                String multiple = entry2.getKey().equals("class") ? "es" : "s";
                System.out.println("  Code-smelling " + entry2.getKey() + multiple + " {");
                for (String s : entry2.getValue()) {
                    System.out.println("    " + s);
                }
                System.out.println("  }");
            }
            System.out.println("}");
        }
    }

    static class FileVisitor extends SimpleFileVisitor<Path> {
        private static Map<String, Map<String, List<String>>> superMap = new HashMap<>();

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            String fullName = file.toAbsolutePath().toString();
            String className = fullName.substring(fullName.indexOf(START_PACKAGE) + START_PACKAGE.length() + 1, fullName.length() - 5);

            Class<?> aClass = null;
            try {
                aClass = Class.forName(className.replaceAll("[\\\\]", "."));
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            assert aClass != null;

            if (aClass.isAnnotationPresent(ThisCodeSmells.class)) {
                countAnnotations("class", aClass.getAnnotation(ThisCodeSmells.class), aClass.getName());
            }

            Field[] fields = aClass.getDeclaredFields();
            for (Field field: fields) {
                if (field.isAnnotationPresent(ThisCodeSmells.class)) {
                    countAnnotations("field", field.getAnnotation(ThisCodeSmells.class), field.toString());
                }
            }

            Method[] methods = aClass.getDeclaredMethods();
            for (Method method: methods) {
                if (method.isAnnotationPresent(ThisCodeSmells.class)) {
                    countAnnotations("method", method.getAnnotation(ThisCodeSmells.class), method.toString());
                }
            }

            Constructor<?>[] constructors = aClass.getDeclaredConstructors();
            for (Constructor<?> constructor : constructors) {
                if (constructor.isAnnotationPresent(ThisCodeSmells.class)) {
                    countAnnotations("constructor", constructor.getAnnotation(ThisCodeSmells.class), constructor.toString());
                }
            }

            return FileVisitResult.CONTINUE;
        }

        private void countAnnotations(String category, ThisCodeSmells annotation, String occasionName) {
            Map<String, List<String>> reviewerMap = superMap.getOrDefault(annotation.reviewer(), new HashMap<>());
            List<String> codeSmellOccasions = reviewerMap.getOrDefault(category, new ArrayList<>());
            codeSmellOccasions.add(occasionName);
            reviewerMap.put(category, codeSmellOccasions);
            superMap.put(annotation.reviewer(), reviewerMap);
        }
    }
}


