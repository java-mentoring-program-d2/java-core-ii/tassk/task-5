import annotations.ProdCode;
import annotations.ThisCodeSmells;
import annotations.processors.ProdRunner;
import annotations.processors.ThisCodeSmellsHandler;
import exceptions.NoVehiclesLeftException;
import exceptions.WrongSortingParametersException;
import vehicles.ElectricBus;
import vehicles.GasBus;
import vehicles.OrdinaryBus;
import vehicles.Vehicle;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@ThisCodeSmells(reviewer = "Ilya Sergeev")
public class VehiclePark {
    private List<Vehicle> allVehicles;
    private long overallVehicleCost;

    public static void main(String[] args) throws IOException {
//        ThisCodeSmellsHandler.handle();

        ProdRunner.handle();

//        VehiclePark park = new VehiclePark();
//        park.createVehicles(new Random().nextInt(10) + 5);
//        park.countOverallCost();
//
//        park.printAllVehicles();
//        List<Vehicle> vehicles = park.searchVehiclesBy(8000, 15500, 50, 75,
//                50, 80);
//        park.printVehicles(vehicles);
    }

    @ProdCode
    private void printAllVehicles() {
        System.out.println("All the vehicles of the park:");
        for (int i = 1; i <= allVehicles.size(); i++) {
            System.out.println(i + ") " + allVehicles.get(i - 1));
        }
        System.out.println("\n======================\n");
    }

    private void printVehicles(List<Vehicle> vehicles) {
        System.out.println("Vehicles satisfying the parameters:");
        for (int i = 1; i <= vehicles.size(); i++) {
            System.out.println(i + ") " + vehicles.get(i - 1));
        }
        System.out.println("\n======================\n");
    }

    private void createVehicles(int amount) {
        allVehicles = new ArrayList<>();
        Random random = new Random();

        for (int i = 0; i < amount; i++) {
            int mass = 5000 + random.nextInt(25000);
            int maxSpeed = 45 + random.nextInt(50);
            int capacity = 30 + random.nextInt(100);
            int typeOrdinal = random.nextInt(3);
            switch (typeOrdinal) {
                case 0: {
                    allVehicles.add(new OrdinaryBus(mass, maxSpeed, capacity));
                    break;
                }
                case 1: {
                    allVehicles.add(new ElectricBus(mass, maxSpeed, capacity));
                    break;
                }
                case 2: {
                    allVehicles.add(new GasBus(mass, maxSpeed, capacity));
                    break;
                }
            }
        }

        Collections.sort(allVehicles);
    }

    private void countOverallCost() {
        overallVehicleCost = allVehicles.stream()
                .map(Vehicle::getCost)
                .reduce(0L, Long::sum);
    }

    public List<Vehicle> getAllVehicles() {
        return allVehicles;
    }

    public long getOverallVehicleCost() {
        return overallVehicleCost;
    }

    @ProdCode
    public List<Vehicle> searchVehiclesBy(int fromMass, int toMass, int fromMaxSpeed, int toMaxSpeed,
                                          int fromCapacity, int toCapacity) {
        if (toMass < fromMass || toMaxSpeed < fromMaxSpeed || toCapacity < fromCapacity) {
            throw new WrongSortingParametersException("You have entered wrong parameters. " +
                    "Right border can't be more than left one.");
        }

        List<Vehicle> vehicles = allVehicles.stream()
                .filter(v -> v.getMass() >= fromMass && v.getMass() <= toMass)
                .filter(v -> v.getMaxSpeed() >= fromMaxSpeed && v.getMaxSpeed() <= toMaxSpeed)
                .filter(v -> v.getCapacity() >= fromCapacity && v.getMaxSpeed() <= toCapacity)
                .collect(Collectors.toList());

        if (vehicles.size() == 0) {
            throw new NoVehiclesLeftException("There are no vehicles according to the sorting parameters");
        } else {
            return vehicles;
        }
    }
}
