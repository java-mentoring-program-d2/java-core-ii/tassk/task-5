package exceptions;

import annotations.ThisCodeSmells;

public final class NoVehiclesLeftException extends VehicleException {
    @ThisCodeSmells(reviewer = "Ilya Sergeev")
    public NoVehiclesLeftException(String message) {
        super(message);
    }
}
